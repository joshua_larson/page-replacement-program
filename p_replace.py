import sys
import time
import os
from itertools import count
iid = count()

clear = lambda: os.system('clear')


class pageTable:
    def __init__(self):
        self.pageDict = {}

    def add(self, pagenum, frame):
        self.pageDict[pagenum] = frame
    def remove(self, pagenum):
        print "Remove Node: " + str(pagenum)
        #del self.pageDict[pagenum]
    def printout(self):
        print "-- Page Number ---- Frame Number --"
        for key in self.pageDict:
            print "|          " + str(key) + "      |      " + str(self.pageDict[key]) + "         |"
        print "-----------------------------------\n"


class physicalMem:
    def __init__(self):
        self.memList= []
        self.inc = 0

# TODO: Change list references to dictionary.  key value is (pname, pagenum), dict value is inc.

    def printout(self):
        print "----------------------------"
        for e in range(0,16,1):

            try:
                print str(e) + " | " + "ID: " + self.memList[e][0][0] + " Page Number: " + str(self.memList[e][0][1]) + "    |"

            except IndexError:
                print str(e) + " |           EMPTY          |"
            print "----------------------------"


    def add(self, (pname, pagenum)):
        print str(self.inc)
        memLoc = 0
        kick = 0
        kicked = 100
        for i in range(0,16,1):

            try:
                if self.memList[i][0] == (pname, pagenum):
                    self.memList[i][1] = self.inc
                    memLoc = i
                    break

                elif i == 15:
                    x = self.memList[0][1]
                    for a in range(0,16,1):
                        if (self.memList[a][1] < x):
                            x = self.memList[a][1]
                            kick = a
                    print '\033[91m' + "Victim is at position " + str(kick) + " in physical memory." + '\033[0m'
                    kicked = self.memList[kick][0][1]
                    print str(self.memList[kick][0][1])
                    self.memList[kick] = [(pname, pagenum), self.inc]
                    kick -= 1
                    memLoc = kick
                    next(iid)
                    break

            except IndexError:
                self.memList.append([(pname, pagenum), self.inc])
                memLoc = len(self.memList)
                next(iid)
                break

        self.inc += 1
        return memLoc, kicked

inputList = []

# Read instructions into memory

f = open(sys.argv[1])

for line in f:
    line = line.split(':')
    line[1] = line[1].strip()
    line[1] = int(line[1],2)
    inputList.append(line)

pMem = physicalMem()
p1 = pageTable()
p2 = pageTable()
p3 = pageTable()
p4 = pageTable()
p5 = pageTable()
clear()
print "***********************************************"
print "Hello, and welcome to the page replacement simulation!\n"

a = raw_input("Ready to begin the simulation? Press the Enter key to continue..\n\nAlternately, you could type 'skip' and hit Enter at any time to \nbypass this introduction. ")
while True:
    clear()
    if a.strip() == 'skip':
        break
    print "***********************************************"
    print "We'll begin by giving a few details about the system..\n"
    print "We are dealing with a machine that has 16KB of physical memory (RAM).\n"
    print "-----------------------------------------------------------------\n|                             16K                               |\n-----------------------------------------------------------------\n"

    print "***********************************************"
    b = raw_input("Press Enter to continue.. ")
    clear()
    if b.strip() == 'skip':
        break
    print "***********************************************"
    print "The 'frame size' of this machine is 1KB/frame.  Meaning that the\nphysical memory is divided into 16 equally sized segments.\n"
    print "-----------------------------------------------------------------\n|1KB|1KB|1KB|1KB|1KB|1KB|1KB|1KB|1KB|1KB|1KB|1KB|1KB|1KB|1KB|1KB|\n-----------------------------------------------------------------\n"

    print "***********************************************"
    c = raw_input("Press Enter to continue..")
    clear()
    if c.strip() == 'skip':
        break
    print "***********************************************"
    print "There are five processes labled p1 through p5. That need to use \nthese frames in order to quickly execute.\n"
    print "However.. each process can need up to 64KB of space to operate!\n"
    print "You're probably asking, 'But, how can five processes that require \n64KB each share only 16KB of memory??'\n"
    print "***********************************************"
    d = raw_input("Press Enter to continue..")
    clear()
    if d.strip() == 'skip':
        break
    print "***********************************************"
    print "The answer is through paging!"
    print "\nYou see, only some parts of a program are in use at a time, so the \nwhole thing doesn't need to be in memory at once.\n"
    print "For example, when you use Microsoft Word or other text editors \nyou only need to " + '\033[1m' + 'bold' + '\033[0m' + " or " +'\033[4m' + 'underline'+ '\033[0m' + " text sometimes."
    print "\nIt wouldn't make sense to keep this code in memory at all times, so \nwhen it is needed it's pulled into memory."
    print "\nOtherwise it's stored in the HDD so it doesn't take up valuable memory space.\n"
    print "***********************************************"
    e = raw_input("Press Enter to continue..")
    clear()
    if e.strip() == 'skip':
        break
    print "***********************************************"
    print "But what is a page?"
    print "\nWell, a page is chunk of code, or a variable, or any part of a process\nthat fits into the size of one frame of memory.  (Remember frames are \nthe segments of physical memory. So, the size of one page is \nthe size of one frame.)"
    print "\nThese pages of a process are swapped in and out of memory by the \noperating system in order to best make use of the limited physical \nmemory of a machine.\n"
    print "***********************************************"
    d = raw_input("Press Enter to continue..")
    clear()
    if d.strip() == 'skip':
        break
    print "***********************************************"
    print "There are several algorithms that the OS can use to swap out memory\nbut this program simulates only the Least Recently Used (LRU) algorithm.\n"
    print "This algorithm works like this:\n"
    print "\t-First, the process makes a reference to a specified page.\n"
    print "\t-If the page is in memory, then whatever is needed from \n\t that page is grabbed and the program continues on as usual.\n\t We also update the time stamp on this memory segment to the current time.\n"
    print "\t-If the page is not in memory, then this is called a 'Page Fault'\n\t and we must go into the HDD to grab the page and put it in memory.\n"
    print "\t-At this point, if the memory is already full, then we must somehow\n\t determine which page in memory to kick out and replace.\n"
    print "\t-With the LRU algorithm, if memory is full, we kick out the page\n\t that we haven't used in the longest time.\n"
    print "Now, we will begin the simulation.."
    print "***********************************************"
    d = raw_input("Press Enter to continue..")
    clear()
    if d.strip() == 'skip':
        break
    break
memoryLocation = 0
finish = False
exit = False
cur = 0
while True:
    if exit == False:
        print "Current Instruction: " + "ID: " + inputList[cur][0] + " :: Page Number: " + str(inputList[cur][1]) + "."
        memoryLocation,kicked = pMem.add((inputList[cur][0],inputList[cur][1]))

        if inputList[cur][0] == 'P1':
            p1.add(inputList[cur][1], memoryLocation)
            if kicked != 100:
                p1.remove(kicked)
        elif inputList[cur][0] == 'P2':
            p2.add(inputList[cur][1], memoryLocation)
            if kicked != 100:
                p2.remove(kicked)
        elif inputList[cur][0] == 'P3':
            p3.add(inputList[cur][1], memoryLocation)
            if kicked != 100:
                p3.remove(kicked)
        elif inputList[cur][0] == 'P4':
            p4.add(inputList[cur][1], memoryLocation)
            if kicked != 100:
                p4.remove(kicked)
        elif inputList[cur][0] == 'P5':
            p5.add(inputList[cur][1], memoryLocation)
            if kicked != 100:
                p5.remove(kicked)


    if cur != 0:
        if skip.strip() == 'exit':
            exit = True
            break
        if skip.strip() == 'P1':
            print "Process: P1\n"
            p1.printout()
        if skip.strip() == 'P2':
            print "Process: P2\n"
            p2.printout()
        if skip.strip() == 'P3':
            print "Process: P3\n"
            p3.printout()
        if skip.strip() == 'P4':
            print "Process: P4\n"
            p4.printout()
        if skip.strip() == 'P5':
            print "Process: P5\n"
            p5.printout()
        if skip == 'finish':
            finish = True
    if cur == len(inputList)-1:
        finish = False
        exit = True
        print "Number of page faults:" + str(iid)
    pMem.printout()
    if finish == False:
        skip = raw_input("Press Enter to continue\nEnter 'exit' to quit\nEnter a process name to view its page table\nEnter 'finish' to complete simulation .. ")
    clear()
    cur += 1







