Page Replacement Simulation Tool

CLI Python script used to demonstrate "least recently used" page replacement to students.

Accepts file with a "memory access" call on each line.
Places data in a 'page' of memory (which is actually just a data structure)
Once a page needs to be deleted, program finds least recently used page and kicks it out so the new page can enter memory.

Simple usage:

python p_replace.py [inputFile]

